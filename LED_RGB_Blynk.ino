/*
   Nama     : Mohamad Ardi
   Kelas    : IoT_4D
   Project  : LED RGB Blynk GOOD
   Date     : 06 Mei 2021
*/
#define BLYNK_PRINT Serial

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

char auth[] = "LxiWurRARTyAzHKXXGprhXW-bBBk8fT-";
char ssid[] = "ESP32";
char pass[] = "mautauaja";

#define led1 12     // Red
#define led2 13     // Green
#define led3 14     // Blue
int PWM_LED1;
int PWM_LED2;
int PWM_LED3;

void setup() {

  ledcAttachPin(led1, 1);      // Tetapkan RGB Led ke Channel1
  ledcAttachPin(led2, 2);      // Tetapkan RGB Led ke Channel2
  ledcAttachPin(led3, 3);      // Tetapkan RGB Led ke Channel3


  ledcSetup(1, 12000, 12);    // 12 kHz PWM, 12-bit resolution
  ledcSetup(2, 12000, 12);    // 12 kHz PWM, 12-bit resolution
  ledcSetup(3, 12000, 12);    // 12 kHz PWM, 12-bit resolution

  Serial.begin(9600);
  Blynk.begin(auth, ssid, pass);
}


BLYNK_WRITE(V0) {
  PWM_LED1 = param.asInt();
  ledcWrite(1, PWM_LED1);      // Write Red Komponen ke Channel 1
}

BLYNK_WRITE(V1) {
  PWM_LED2 = param.asInt();
  ledcWrite(2, PWM_LED2);      // Write Green Komponen ke Channel 2
}

BLYNK_WRITE(V2) {
  PWM_LED3 = param.asInt();
  ledcWrite(3, PWM_LED3);      // Write Blue Komponen ke Channel 3
}

void loop() {
  Blynk.run();
}
